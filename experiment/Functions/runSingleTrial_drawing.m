function [data,eyeData, mouseData] = runSingleTrial(td)
% 2016 by Martin Rolfs 
% 2021 by Jan Klanke
% 
% Input:  td      - part of the design structure that pertains to the
%                   trial to be displayed
% 
% Output: data    - trial parameters and behavioral results
%         eyeData - flag for eyetracking  

global scr visual setting keys mouse

% clear keyboard buffer
FlushEvents('KeyDown');
if ~setting.TEST == 1, HideCursor(); end
pixx = setting.Pixx; 

mouse.slowF = 2;

% Set the transparency for the stimulus (Gabor patch or sine w/ raised
% cosine masketc.)
% Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% predefine boundary information
cxm = td.bound.pos;
cym = td.bound.pos;
rad = td.bound.rad;
chk = td.bound.rad;

% draw trial information on operator screen
if ~setting.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm(1,  1)-chk(1))*2, (cym(1,  2)-chk(1))*2, (cxm(1,  1)+chk(1))*2, (cym(1,  2)+chk(1))*2); end
if ~setting.TEST, Eyelink('command','draw_box %d %d %d %d 15', (cxm(end,1)-chk(1))*2, (cym(end,2)-chk(1))*2, (cxm(end,1)+chk(1))*2, (cym(end,2)+chk(1))*2); end

% generate donut-shaped stimulus
nStim = length(td.stims.pars.lenp);
sti.tex = visual.procStimsTex;
sti.src = [zeros(2,nStim); td.stims.pars.lenp; td.stims.pars.higp];
sti.dst = CenterRectOnPoint(sti.src, td.stims.locX', td.stims.locY');
for f = 1:td.totNFr; stiFrames(f).dst = sti.dst + repmat(reshape(td.posVec(:,f),[],nStim),nStim,1); end

% predefine time stamps
tFixaOn = NaN;  % t of fixation stream on
tStimOn = NaN;  % t of stimulus stream on
tStimCf = NaN;  % t of stimulus at full contrast
tStimMS = NaN;  % t of stimulus starting to move
tStimMT = NaN;  % t of stimulus at peak velocity
tStimCd = NaN;  % t of stimulus contrast starts to decline
tStimOf = NaN;  % t of stimulus off
tRes    = NaN;  % t of response (if any)
tClr    = NaN;  % t of of clear screen

% set flags before starting stimulus stream
eyePhase  = 1;  % 1 is fixation phase, 2 is saccade phase
breakIt   = 0;
fixBreak  = 0;
saccade   = 0;

% Data
mouseData  = [];
eyeData    = [];
x1 = NaN;
xn = NaN;
y1 = NaN;
yn = NaN;
xd = NaN;
yd = NaN;
scrHalf = NaN;
mouse_dist = NaN;
mouse_angle1 = NaN;
mouse_amp = NaN;
mouse_angle2 = NaN;
max_curvature = NaN;


% Initialize vector important for response
pressed = 0;
resTier = 1;   
response= NaN(1,3);

% Initialize vector to store data on timing
frameTimes = repmat(12*scr.fd,1,td.totNFr);

% flip screen to start out time counter for stimulus frames
firstFlip = 0;nextFlip = 0;
while firstFlip == 0
    if pixx, firstFlip = PsychProPixx('QueueImage', scr.myimg);
    else     firstFlip = Screen('Flip', scr.myimg); end
end

% set frame count to 0
f = 0;              

% get a first timestap
tLoopBeg = GetSecs; 
t = tLoopBeg;

while ~breakIt && f < td.totNFr
    f = f+1;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % stimulus presentation %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
    for slo = 1:setting.sloMo
        Screen('FillRect', scr.myimg, visual.bgColor);
        % fixation
        if td.fixa.vis(f)
           drawFixation(td.fixa.col, td.fixa.loc, td.fixa.sin, scr.myimg) 
        end
        % target
        if td.targ.vis(f)
           drawFixation(td.targ.col, td.targ.loc(f,:), td.targ.siz, scr.myimg)     
           % Screen('FrameOval', scr.myimg, scr.black, [-rad(f) -rad(f) rad(f) rad(f)] + [cxm(f,1) cym(f,2) cxm(f,1) cym(f,2)], [], [], []);
        end
        % stimulus
        % Screen('BlendFunction', scr.myimg, GL_SRC_ALPHA, GL_ONE);
        if td.stims.vis(f)
            Screen('DrawTextures', scr.myimg, repmat(sti.tex,nStim,1), sti.src, stiFrames(f).dst, td.stims.pars.ori', [], td.stims.amp(:,f)', [], [], [], [zeros(1,nStim); td.stims.pha(:,f)'; zeros(1,nStim); zeros(1,nStim)]);
        end
        % flip
        if pixx; nextFlip = PsychProPixx('QueueImage', scr.myimg);
        else     nextFlip = Screen('Flip', scr.myimg); end
    end
    frameTimes(f) = frameTimes(f) + GetSecs;   
    
    %%%%%%%%%%%%%%%%%%%%%%%%
    % raise stimulus flags %
    %%%%%%%%%%%%%%%%%%%%%%%%
    % Send message that stimulus is now on
    if isnan(tFixaOn) && td.events(f)==1
        if ~setting.TEST  ; Eyelink('message', 'EVENT_FixaOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_FixaOn'); end
        tFixaOn = frameTimes(f);
    end
    if isnan(tStimOn) && td.events(f)==2
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOn'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOn'); end
        tStimOn = frameTimes(f);
    end
    if isnan(tStimCf) && td.events(f)==3
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCf'); end
        tStimCf = frameTimes(f);
    end
    if isnan(tStimMS) && td.events(f)==4
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMS'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMS'); end
        tStimMS = frameTimes(f);
    end
    if isnan(tStimMT) && td.events(f)==5
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimMT'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimMT'); end
        tStimMT = frameTimes(f);
    end
    if isnan(tStimCd) && td.events(f)==6
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimCd'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimCd'); end
        tStimCd = frameTimes(f);
    end
    if isnan(tStimOf) && td.events(f)==7
        if ~setting.TEST  ; Eyelink('message', 'EVENT_StimOf'); end
        if  setting.TEST>1; fprintf(1,'\nEVENT_StimOf'); end
        tStimOf = frameTimes(f);
    end
    
    % eye position check
    if setting.TEST<2
        [xl,xr,yl,yr] = getCoords;
        if sqrt((xr-cxm(f,1))^2+(yr-cym(f,2))^2)>chk(f) || sqrt((xl-cxm(f,1))^2+(yl-cym(f,2))^2)>chk(f)   % check fixation in a circular area
            fixBreak = 1;
        end
    end
    if fixBreak
        breakIt = 1;    % fixation break
    end
end

lastFlip = 0;
while lastFlip == 0 && nextFlip == 0
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, lastFlip = PsychProPixx('QueueImage', scr.myimg);
    else     lastFlip = Screen('Flip', scr.myimg); end 
end
tLoopEnd = GetSecs;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% stimulus presentation 'Blank' %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Screen('BlendFunction', scr.myimg, GL_ONE, GL_ZERO);
for i = 1:scr.rate
    Screen('FillRect', scr.myimg, visual.bgColor);
    if pixx, PsychProPixx('QueueImage', scr.myimg);
    else     Screen('Flip', scr.myimg); end
end
WaitSecs(td.aftKey/4);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Break trial or initiate reponse query %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nextRespFlip = 0; 
switch breakIt
    case 1
        % data = 'fixBreak';
        data = 'fixBreak';
        if setting.TEST<2, Eyelink('command','draw_text 100 100 15 Fixation break'); end
    otherwise
        
        % Set init parameters
        tResBeg = GetSecs;delta=0;ticks = td.sOnPos;
        
        % response phase
        pressed = 0;
        while ~pressed   
            
            % evaluate response to first 2 questions
            if resTier <= 2
                if ( td.scale.Dir && checkTarPress(keys.resRghtA)) || (~td.scale.Dir && checkTarPress(keys.resLeftA)) 
                    response(resTier) = 1; 
                    resTier = resTier + 1; 
                    WaitSecs(td.aftKey);
                end
                if (~td.scale.Dir && checkTarPress(keys.resRghtA)) || ( td.scale.Dir && checkTarPress(keys.resLeftA))
                    response(resTier) = 0; 
                    resTier = resTier + 1; 
                    WaitSecs(td.aftKey); 
                end
            end
            
            % create response graphics for scale
            for i = 1:scr.rate
                Screen('FillRect', scr.myimg, visual.bgColor);         % draw background
                if resTier <= 2; drawScale(ticks, td.scale, resTier); end
                if pixx, nextRespFlip = PsychProPixx('QueueImage', scr.myimg);
                else     nextRespFlip = Screen('Flip', scr.myimg); end
            end
            
            % end-clause
            if resTier > 2; pressed = 1; end
        end

        % evaluate response to last 2 questions
        lastFlip  = 0;
        thePoints = [];
        if resTier > 2 && response(1) == 1
            
            % set mouse
            realX = scr.rect(3);
            realY = scr.rect(4);
            SetMouse(realX, realY, scr.expScreen);
            
            phase = 1;
            while 1
                [x,y,buttons] = GetMouse(scr.main);
                                              
                switch phase
                    
                    % draw pre-drawing phase
                    case 1
                        dispX = scr.centerX+(x-scr.rect(3))/mouse.slowF;
                        dispY = scr.centerY+(y-scr.rect(4))/mouse.slowF;
                        Screen('FillRect', scr.myimg,visual.bgColor);
                        Screen('DrawText',scr.myimg,'Drag mouse (i.e. hold button down) to draw:',50,50,0);
                        Screen('DrawDots', scr.myimg, [dispX dispY], 3, visual.black, [], 2);
                        if buttons(1) 
                            phase     =  2; % switch to next phase is button is pressed
                            thePoints = [dispX dispY];
                        end
                        
                    % draw drawing phase    
                    case 2
                                              
                        % compare and update mouse position
                        if (x ~= realX || y ~= realY) 
                            realX = x; realY = y; 
                            dispX = scr.centerX+(realX-scr.rect(3))/mouse.slowF;
                            dispY = scr.centerY+(realY-scr.rect(4))/mouse.slowF;
                            thePoints = [thePoints; dispX dispY];

                            % draw arrow head
                            dist  = sqrt((thePoints(end,1)-thePoints(1,1))^2+(thePoints(end,2)-thePoints(1,2))^2); 
                            theta =  -(89 * sign(dist)*(pi/180)) + atan2((thePoints(end,2)-thePoints(1,2)),(thePoints(end,1)-thePoints(1,1)));   % -(90 * sign(dist))*(pi/180);
                            head  = thePoints(end,:);	    % coordinates of head
                            width = 10;                     % width of arrow head
                            verticies = [ head-[width+sign(dist)*2,0]; head+[width-sign(dist)*2,0]; head+[0,width] ];
                            centerPt  = mean(verticies);
                            coordsX = [centerPt(1) + ((verticies(:,1)-centerPt(1))*cos(theta) - (verticies(:,2)-centerPt(2))*sin(theta))];
                            coordsY = [centerPt(2) + ((verticies(:,1)-centerPt(1))*sin(theta) + (verticies(:,2)-centerPt(2))*cos(theta))];    
                            rot_verticies = [coordsX,coordsY] + [sign(dist) -4];
                            Screen('FillRect', scr.myimg,visual.bgColor);
                            Screen('DrawText',scr.myimg,'Drag mouse (i.e. hold button down) to draw:',50,50,0);
                            % Screen('DrawDots', scr.myimg, thePoints(1:end-1,:)', 4, visual.black);
                            Screen('DrawDots', scr.myimg, thePoints', 4, visual.black);
                            Screen('FillPoly', scr.myimg, visual.black, rot_verticies);
                        end
                        
                        % ...we ask Flip to not clear the framebuffer after flipping:
                        if ~buttons(1); phase =  3; end % switch to next phase is button is un-pressed
                        
                    % switch to post-drawing phase    
                    case 3
                        Screen('FillRect', scr.myimg,visual.bgColor);
                        Screen('DrawText',scr.myimg,'Is this the trajectory you wanted to draw?',50,50,0);
                        if ~isempty(thePoints) && exist('rot_verticies', 'var')
                            Screen('DrawDots', scr.myimg, thePoints', 4, visual.black);
                            Screen('FillPoly', scr.myimg, visual.black, rot_verticies);
                        end
                        for i = 1:length(td.scale.lvl(1).a.labels); drawText(td.scale.lvl(1).a.labels{i},td.scale.lvl(1).a.posP(i,1)-250,td.scale.lvl(1).a.posP(i,2)-200); end
                        if (td.scale.Dir && checkTarPress(keys.resRghtA)) || (~td.scale.Dir && checkTarPress(keys.resLeftA)) 
                            phase = 4;
                        elseif (~td.scale.Dir && checkTarPress(keys.resRghtA)) || ( td.scale.Dir && checkTarPress(keys.resLeftA))
                            phase = 1; % restart phase 1 if person is unhappy abt drawing
                            thePoints = [];
                        end
                        
                    % phsae 4 means you arew done you are done    
                    case 4
                        break;
                end
                
                if pixx, PsychProPixx('QueueImage', scr.myimg);
                else     Screen('Flip', scr.myimg); end 
            end
            HideCursor(scr.main);
        end
            
        while ~lastFlip
            lastFlip = PsychProPixx('QueueImage', scr.myimg);
        end    

        % mouse.thePoints = thePoints;
        mouseData = thePoints;
        
        % get timing when participants are done
        tRes = GetSecs();
        
        % Fill up buffer 
        lastRespFlip = 0;
        while lastRespFlip == 0 && nextRespFlip == 0
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, lastRespFlip = PsychProPixx('QueueImage', scr.myimg);
            else     lastRespFlip = Screen('Flip', scr.myimg); end 
        end
        
        % make one flip with empty screen
        for i = 1:scr.rate
            Screen('FillRect', scr.myimg, visual.bgColor);
            if pixx, PsychProPixx('QueueImage', scr.myimg);
            else     Screen('Flip', scr.myimg); end
        end
        
        % determine response duration 
        tResDur = (tRes - tLoopBeg) * 1000; 
                             
        WaitSecs(td.aftKey);       
        tClr = GetSecs;
        if setting.TEST<2, Eyelink('message', 'EVENT_Clr'); end
        if setting.TEST; fprintf(1,'\nEVENT_Clr'); end 

        
        % pre-analyze mouse data
        if ~isempty(mouseData)
            % 1a. Mouse start/endpoints
            x1 = mouseData(1,1);
            xn = mouseData(end,1);
            y1 = mouseData(1,2);
            yn = mouseData(end,2);

            % 1b. max. distance points
            [minx, ix1] = min(mouseData(:,1));
            [maxx, ix2] = max(mouseData(:,1));
            [miny, iy1] = min(mouseData(:,2));
            [maxy, iy2] = max(mouseData(:,2));

            % 3. Mouse direction
            xd = sign(maxx - minx);   % 1 = right, -1 = left
            yd = sign(maxy - miny);   % 1 = right, -1 = left

            % 4. upper or lower screen-half
            if mean(mouseData(:,2)) < scr.rect(4)
                scrHalf = 1; % upper half
            else
                scrHalf = -1;% lower half
            end

            % 5. Mouse distance
            dx = mouseData(end,1) - mouseData(1,1);
            dy = mouseData(end,2) - mouseData(1,2);
            mouse_dist   = sqrt(dx+dy);
            mouse_angle1 = atan2(dy,dx);

            % 6. Mouse amplitude
            dX     = sign(ix2 - ix1)*(maxx - minx);
            dY     = sign(iy2 - iy1)*(maxy - miny);
            mouse_amp     = sqrt(dX+dY);
            mouse_angle2 = atan2(dY,dX);

            % 7. Mouse curvatur
            deriv_x  = gradient(mouseData(:, 1)); % first deriv.
            deriv_y  = gradient(mouseData(:, 2));
            deriv2_x = gradient(deriv_x);
            deriv2_y = gradient(deriv_y);
            curvature = abs(deriv_x .* deriv2_y - deriv_y .* deriv2_x) ./ (deriv_y.^2 + deriv_y.^2).^(3/2);
            max_curvature = max(curvature(isfinite(curvature)));
        end
                
        %-------------------------%
        % PREPARE DATA FOR OUTPUT %
        %-------------------------%
        % collect trial information
        condData = sprintf('%i\t%i',...
            [td.expcon setting.train]);                                     % in results, cells  6:8
        
        % collect stimulus information                                        in results, cells 9:20
        stimData = sprintf('%.2f\t%.2f\t%i\t%.2f\t%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%i\t%.2f\t%.2f',...  
            [td.fixpox td.fixpoy td.movdir td.movvel td.stioriUP td.stioriDW td.phavel td.appbeg td.apptop td.appdur td.appmdX td.appmdY td.appori td.appvpk td.appamp]);
        
        % collect data on the artificial saccade that shaped the stim.        in results, cells 21:26
        asacData = sprintf('%i\t%i\t%.2f\t%.2f\t%.2f\t%.2f',...
            [td.sac.idx td.sac.req td.sac.amp td.sac.vpk td.sac.dur td.sac.ang]);
        
        % collect scale information                                           in results, cells 27:31
        scaleData = sprintf('%i\t%i',...
            [td.scale.Dir td.sOnPos]); 
                           
        % collect time information                                            in results, cells 32:39
        timeData = sprintf('%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i\t%i',...                             
            round(1000*([tFixaOn tStimOn tStimCf tStimMS tStimMT tStimCd tStimOf tRes tClr]-tStimOn)));
        
        % collect response information                                        in results, cells 40:43
        respData = sprintf('%i\t%i%' , ...% \t%.2f\t%.2f\t%.2f\t%.2f\t%i\t%i\t%i\t%.2f\t%.4f\t%.2f\t%.4f\t%.2f',... 
            response(1), response(2)); %  x1, xn, y1, yn, xd, yd, scrHalf, mouse_dist, mouse_angle1, mouse_amp, mouse_angle2, max_curvature );

        % get information about how timing of frames worke                    in results, cells 44:45
        tLoopFrames = round((tLoopEnd-tLoopBeg)/scr.fd);        
        frameData = sprintf('%i\t%i',tLoopFrames,td.totNFr);

        % collect data for tab [3 x condData, 10 x trialData, 5 x sacData, 10 x timeData %i, 8 x respData, 2 x frameData]
        data = sprintf('%s\t%s\t%s\t%s\t%s\t%s\t%s',...
            condData, stimData, asacData, scaleData, timeData, respData, frameData);
end
end