function param = prepReplayCondition(vpno, seno, coow)

% reads in the various data for the ms conditions and makes sure that
% everything is as it should be
% input: vp number, session number
% output: array with ms coords, velocities, onsets, and fitted main
% sequence parameters
%
% 06/2020 by Jan Klanke

while 1
    
    % function to easily filter the names of the files for a certain pattern.
    sf = @(cll,ptn) ~cellfun('isempty', regexp(cll,ptn));

    % get last session no of completed sessions
    if coow==1; sesnum = 0;
    else sesnum = str2double(seno) - 1; end

    % predefine ms structures
    param = [];

    % filter files of participant
    fitData =  dir(sprintf('Data/fitData/SPC2_%s0%i.csv',vpno, sesnum));

    % switch for session number
    switch sesnum
        case 0
            fprintf(1,'\n>>>> Saccade data status: no saccade data');
            fprintf(1,'\n>>>> ...proceed...\n');
            WaitSecs(0.5);
            break; % break w/o stop

        otherwise
            if isempty(fitData)
                fprintf(1,'\n>>>> Unable to load data for this participant.');
                fprintf(1,'\n>>>> Please make sure to use the correct subject id and session no,');
                fprintf(1,'\n>>>> and that all necessary files are downloaded from the repo.');
                fprintf(1,'\n>>>> ...abort...\n');
                WaitSecs(0.5);
                error('1');
            end
            
            % load in ms data of all available files
            rawData  = importdata(fitData.name, ',');
            targetID = rawData.textdata(2,2);
            targetSN = rawData.data(1,1);
            
            for i = 1:size(rawData.data,1)
                param(i).tarvel = rawData.data(i,2);
                param(i).shape  = rawData.data(i,3);
                param(i).rate   = rawData.data(i,4);
                param(i).scale  = 1/param(i).rate;
                param(i).V0     = rawData.data(i,5);
                param(i).A0     = rawData.data(i,6);
            end

            % communicate successful load
            if targetSN == 1; targetSN_text = 'session 1'; else targetSN_text = sprintf('sessions 1 to %i', targetSN); end
            while ~isempty(param)
                    % show files
                    fprintf(1,'\n>>>> I was able to load the data for participant %s, %s', targetID{1}, targetSN_text);
                    fprintf(1,'\n>>>> (*) %s (created %s)', (fitData.name), extractBefore(fitData.date, ' '));
                    finalAnswer = input('\n\n>>>> Do you want to continue [y / n]? ','s');

                    % allow experimenter to decide whether they want to
                    % continue
                    if strcmp(finalAnswer,'y')
                        fprintf(1,'>>>> ...proceed...\n');
                        WaitSecs(0.5); 
                        break; % break w/o flag stop
                    else
                        param = [];
                    end
            end
            
            % check whether files could be loaded successfully and abort if
            % not.
            if isempty(param)
                fprintf(1,'\n>>>> I was not able to load these files...');
                fprintf(1,'\n>>>> ...abort...\n');
                error('1');
            end
            
            % do a final break if experimenter has prev agreed
            if strcmp(finalAnswer,'y'); break; end  % break W/O STOP FLAG 
    end
end

end

