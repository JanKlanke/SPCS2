function [curve_M,rot_M,path_M] = mouse_identifier(mousedata,fix_loc)


mousedata = [mousedata(:,2),mousedata(:,3)];
coord_diff = mousedata(end,:)-mousedata(1,:); % get the difference between the start-end positions

% below we find whether the response is vertical or horizontal
    if abs(coord_diff(1))<abs(coord_diff(2)) % if x < y differences
        respHor = 0; % vertical
    elseif abs(coord_diff(1))>abs(coord_diff(2)) % if x > y diff.
        respHor = 1; % horizontal
    elseif abs(coord_diff(1))==abs(coord_diff(2)) % if x=y diff. - probably an accidental click
        respHor = NaN;
    end


    % define the axis

    if respHor == 0 % if vertical
        xy = 1;
    else %horizontal
        xy = 2;
    end

    % define the path

    base = mean([mousedata(1,xy), mousedata(end,xy)]);


    %     dc = data_design.design.b.trial.stims.dot_coord';
    %     dot_coord = dc(xy,1);

    if base > fix_loc(xy)
        if respHor == 0
            path_M = 3; %right
        else
            path_M = 1; %bottom
        end
    elseif base < fix_loc(xy)
        if respHor == 0
            path_M = 4;%left
        else
            path_M = 2; %top
        end
    elseif base == fix_loc(xy)
        path_M = nan;
    end

    % define the curvature

    A = base - max(mousedata(:,xy));
    B = base - min(mousedata(:,xy));
    if ((abs(A) > abs(B)) && mod(path_M, 2) == 1) || ((abs(B) > abs(A)) && mod(path_M, 2) == 0)
        curve_M  = 1; %out
    elseif ((abs(A) > abs(B)) && mod(path_M, 2) == 0) || ((abs(B) > abs(A)) && mod(path_M, 2) == 1)
        curve_M  = -1; %in
    elseif abs(A) == abs(B)
        curve_M  = nan;
    end

    % cw-ccw - depends on the sign of X(for horz) or Y(for vert) on
    % coord_diff and the position of the response
    rot_M = 0; % ccw trials
    if respHor % if horizontal
        if sign(coord_diff(1)) == -1 && path_M == 1 || sign(coord_diff(1)) == 1 && path_M == 2
            rot_M = 1; % cw
        end
    elseif ~respHor
        if sign(coord_diff(2)) == -1 && path_M == 4 || sign(coord_diff(2)) == 1 && path_M == 3
            rot_M = 1; % cw
        end

    end

end