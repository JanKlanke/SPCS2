function [xl,xr,yl,yr] = getCoords

global setting scr

if setting.TEST
	[xl,yl,~] = GetMouse( scr.main );         % get gaze position from mouse for LEFT eye		
    xr = xl; yr = yl;                         % get gaze position from mouse for RIGHT eye					
else
    leftEye  = 1; 
    rightEye = 2;
	evt = Eyelink('newestfloatsample');	
	xl   = evt.gx(leftEye)/2;	yl   = evt.gy(leftEye)/2;		
    xr   = evt.gx(rightEye)/2;	yr   = evt.gy(rightEye)/2;	
end
