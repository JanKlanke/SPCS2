function drawScale(scl, rT)
% 2020 by Jan Klanke
% 
% Input:  p   - index of the position at which the scale hand should be drawn
%         scl - parameters of the scale
%         rT  - response tier; i.e. which question/response level is displayed



% Draw labels for the scale.
scl.lvl(rT).a.labels = reshape(scl.lvl(rT).a.labels',1,[]);

for i= 1:numel(scl.lvl(rT).q.labels); drawText(scl.lvl(rT).q.labels{i},scl.lvl(rT).q.posP(i,1),scl.lvl(rT).q.posP(i,2)); end
for i= 1:numel(scl.lvl(rT).a.labels); drawText(scl.lvl(rT).a.labels{i},scl.lvl(rT).a.posP(i,1),scl.lvl(rT).a.posP(i,2)); end

end