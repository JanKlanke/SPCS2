function getMouseTrajectory 

% 2022 by Melis Ince 
% adapted 2023 by Jan Klanke


global scr mouse 
% Screen('Preference', 'SkipSyncTests', 1); % only for mac - M1 compatibility issue 


% Move the cursor to the center of the screen
SetMouse(scr.centerX, scr.centerY, scr.expScreen);

    % Wait for a click and hide the cursor
    Screen(scr.myimg,'FillRect',0);
    Screen(scr.myimg,'TextSize',24);
    Screen(scr.myimg,'DrawText','Drag mouse (i.e. hold button down) to draw',50,50,255);
    PsychProPixx('QueueImage', scr.myimg);
    while 1
        [~,~,buttons] = GetMouse(scr.main);
        if buttons(1)
          break;
        end
    end
    Screen(scr.myimg,'DrawText','Release button to finish',50,50,255);

    % Loop and track the mouse, drawing the contour
    [scr.centerX,scr.centerY] = GetMouse(scr.myimg);
    mouse.mouse.thePoints = [scr.centerX scr.centerY];
    Screen(scr.myimg,'DrawLine',255,scr.centerX,scr.centerY,scr.centerX,scr.centerY);
    
    % Set the 'dontclear' flag of Flip to 1 to prevent erasing the
    % frame-buffer:
    PsychProPixx('QueueImage', scr.myimg);

    while (1)
        [x,y,buttons] = GetMouse(scr.myimg);	
        if ~buttons(1)
            break;
        end
        if (x ~= scr.centerX || y ~= scr.centerY)
            mouse.thePoints = [mouse.thePoints ; x y]; %#ok<AGROW>
            [numPoints, ~]= size(mouse.thePoints);
            % Only draw the most recent line segment: This is possible,
            % because...
            Screen(scr.myimg,'DrawLine',128,mouse.thePoints(numPoints-1,1),mouse.thePoints(numPoints-1,2),mouse.thePoints(numPoints,1),mouse.thePoints(numPoints,2));
            % ...we ask Flip to not clear the framebuffer after flipping:
            PsychProPixx('QueueImage', scr.myimg);
            scr.centerX = x; scr.centerY = y;
        end
    end

