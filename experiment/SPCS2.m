%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Saccade awareness for Anit-Saccades   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% 2023 by Jan Klanke
%
% 
% DESKRIPTION
%
%
%
% TODOs
%
%
%

clear all;
clear mex;
clear functions; 
delete(instrfindall);

addpath('Functions/','Data/trialData', 'Data/fitData'); 

home;
expStart=tic;


global setting visual scr keys %#ok<*NUSED>

setting.train= 0;         % do you want to run a pracitce block?

setting.TEST = 0;         % test in dummy mode? 0=with eyelink; 1=mouse as eye; 2=no gaze position checking of any sort
setting.sdir = 0;         % direction of the response schema: 0 = yes->no; 1= no->yes NaN-> auto-assignment in prepExp

setting.pilot= 0;         % are you piloting (1) or not (0)?
setting.Pixx = 1;         % Are you using the propixx (1) or not (0) - do you want to simulate the propixx (2)
setting.sloMo= 1;         % To play stimulus in slow motion, draw every frame setting.sloMo times

exptname = 'SPC2';

try 
    newFile = 0;
    while ~newFile
        [vpno, seno, cond, dome, subjectCode, overrideSwitch] = prepExp(exptname);
        datFiles = {sprintf('Data/trialData/%s.dat',subjectCode),sprintf('Data/%s',subjectCode)};
        
        % create data file
        if exist(datFiles{1},'file')
            o = input('>>>> This file exists already. Should I overwrite it [y / n]? ','s');
            if strcmp(o,'y')
                newFile = 1;
            end
        else
            newFile = 1;
        end
    end
    
    % prepare replay condition
    param = prepReplayCondition(vpno, seno, overrideSwitch);
    
    % prepare screens
    prepScreen;

    % get key assignment
    getKeyAssignment;
    
    % disable keyboard
    ListenChar(2);
          
    % prepare stimuli
    prepStim;
    
    % generate design
    design = genDesign(vpno, seno, cond, dome, param, overrideSwitch, datFiles{2});
    
    % initialize eyelink-connection
    if setting.TEST<2
        [el, err]=initEyelinkNew(subjectCode(end-7:end));
        if err==el.TERMINATE_KEY
            return
        end
    else
        el=[];
    end
    
    % runtrials
    design = runTrials(design,datFiles,el);
    
    % shut down everything
    reddUp;
    
catch me
    rethrow(me); 
    reddUp; %#ok<UNRCH>
end

expDur=toc(expStart);

fprintf(1,'\n\nThis (part of the) experiment took %.0f min.',(expDur)/60);
fprintf(1,'\n\nOK!\n');

% plotReplayResults(exptname, vpno, seno);